# Lost in the Woods 

## Description

Lost in the Woods is a game based on the Godot-Engine of a game-boy styöe game.

- You walk around trying to .
- Accept invitations whenever you want.
- Send an add-request to your people with your "name#code".
- No registration required

## Structure

Lost in the Woods structure is divided by:

**Stand: V.0.1**:

- Wireframes: Wireframes from all necessary game visual structure. 
- Lost in the Woods: Godot engine - 2D project

## Requirements

**Godot**:

- Godot game-engine official-website [download ](https://godotengine.org/download/)

## Tech/framework used

**Stand: V.0.1.**:

- Game Development - [Godot game-engine](https://reactnative.dev/)
- Webpage - [Angular](https://angular.io/)
- Backend - [Firebase](https://firebase.google.com/)

## Contribution

Pull & push requests are welcome! Just remember to check if you have the updated pull before pushing the code.

## Project-Management System

**Stand V.0.1.**:

- [Trello board](https://trello.com/invite/b/8dr4OYEi/2b6abe85d99c1e9f23604a3c3d667d43/godot-lost-in-the-woods) [Public access]

- [Gitlab]https://gitlab.com/Steamless/godot_gb

## README

- [README Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

- [README.md knowhow 101](https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3)

***After any big change, pls remember to add it to the README***

**Happy coding! ^^**

--------------------------------------------------------------------------------

# License Information

The copyright of the media and media-related type of content or file from this repository is protected under the 'CC BY-NC' license as **Fernando Franke**, the only creator of the content.

##### This means that you are free to share and adapt the content for non-commercial purposes only, as long as you give appropriate credit to Fernando and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.  
<br>
For more information, please refere to:  

- [CC BY-NC License](https://creativecommons.org/licenses/by-nc/4.0/)
- [Fernando Franke - itch.io](https://steamless.itch.io/)


--------------------------------------------------------------------------------

