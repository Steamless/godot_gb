extends CanvasLayer

export(String, FILE, "*.json") var text_npc_file

var dialoges = []
var current_dialoge_id = 0
var is_active = false

func _ready():
	$NinePatchRect.visible = false
	$NinePatchRect2.visible = false

func play():	
	if is_active:
		return
	
	dialoges = load_dialoges()
	
	is_active = true
	$NinePatchRect.visible = true
	$NinePatchRect2.visible = true
	
	current_dialoge_id = -1
	next_line()

func _input(event):
	if not is_active:
		return
	
	if event.is_action_pressed("ui_accion"):
		next_line()

func next_line():
	current_dialoge_id += 1	
	if current_dialoge_id >= len(dialoges):
		$Timer.start()
		$NinePatchRect.visible = false
		$NinePatchRect2.visible = false
		
		return
	
	$NinePatchRect/title.text = dialoges[current_dialoge_id]['title']
	$NinePatchRect/body.text = dialoges[current_dialoge_id]['body']

func load_dialoges():
	var file = File.new()
	file.open(text_npc_file, File.READ)
	
	var content = file.get_as_text()
	return parse_json(content) 

func _on_Timer_timeout():
	is_active = false

#TODO: Implement not moving player when triggered
