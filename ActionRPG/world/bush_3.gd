extends Node2D

const bushEffect = preload("res://effects/bush_hit_effect.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func create_bush_effect():
	var bushEffectNode = bushEffect.instance() #instance scene
		
	get_parent().add_child(bushEffectNode) # adding scene as child to whatever node you need access to.
		
	# global position is the bush position. So effect seton bush position.
	bushEffectNode.global_position = global_position


func _on_Hit_Hurtbox_area_entered(area):
	create_bush_effect()
	queue_free()


func _on_Accion_Hurtbox_area_entered(area):
	getDialogeFile()
	

func getDialogeFile():
	var dialoge_cuasi = get_node_or_null("message_dialoge")
	
	if dialoge_cuasi:
		dialoge_cuasi.play()
		
