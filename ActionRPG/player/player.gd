extends KinematicBody2D

export var ACCELERATION = 500
export var MAX_SPEED = 80
export var FRICTION = 500

enum {
	MOVE,
	ATTACK,
	HIT,
	DEAD,
	ACCION
}

var state = MOVE
var velocity = Vector2.ZERO
var hit_vector = Vector2.LEFT
var stats = PlayerStats

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback") # To get acces to the animation tree animations
onready var hit_hitbox = $Hit_Hitbox_Pivot/hit_hitbox

func _ready():
	stats.connect("no_health", self, "queue_free")
	#stats.connect("no_health", self, "dead_state")
	
	animationTree.active = true
	hit_hitbox.knockback_vector = hit_vector

func _process(delta):
	match state:
		MOVE:
			move_state(delta)
		
		ATTACK:
			attack_state(delta)
		
		HIT:
			hit_state(delta)
		
		DEAD:
			dead_state(delta)
		
		ACCION:
			accion_state(delta)


func move_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized() # To pin the diagonal vector, to make the speed to 1
	# print(input_vector)
	
	if input_vector != Vector2.ZERO:
		hit_vector = input_vector
		hit_hitbox.knockback_vector = input_vector
		animationTree.set("parameters/idle/blend_position", input_vector)
		animationTree.set("parameters/walk/blend_position", input_vector)
		animationTree.set("parameters/attack/blend_position", input_vector)
		animationTree.set("parameters/accion/blend_position", input_vector)
		animationState.travel("walk")
		
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
		
	else:
		animationState.travel("idle")
		
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
	  
	#print(velocity)
	move()
	
	# just_pressed Statements
	if Input.is_action_just_pressed("ui_attack"):
		state = ATTACK
	elif Input.is_action_just_pressed("ui_accion"):
		state = ACCION


func move():
	velocity = move_and_slide(velocity)

# States Animation
func accion_state(delta):
	animationState.travel("accion")
	print("accion pressed")
	
func attack_state(delta):
	animationState.travel("attack")
	print("hit pressed")
	
func hit_state(delta):
	animationState.travel("hit")
	
func dead_state(delta):	
	animationState.travel("")
	#queue_free()


# Finish State -> Move
func attack_animation_finished():
	state = MOVE
	
func accion_animation_finished():
	state = MOVE

func hit_animation_finished():
	state = MOVE

func _on_Hurtbox_area_entered(area):
	stats.health -= 1
