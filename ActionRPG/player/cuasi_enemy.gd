extends KinematicBody2D

const EnemyDeathEffect = preload("res://effects/enemy_death_effect.tscn")

export var ACCELERATION = 300
export var MAX_SPEED = 50
export var FRICTION = 200

enum {
	IDLE,
	WANDER,
	CHASE,
	DEAD
}

var velocity = Vector2.ZERO
var knockback = Vector2.ZERO

var state = IDLE
var isDead = false

onready var stats = $Stats
onready var playerDetectionZone = $player_detection_zone
onready var animatedSprite = $AnimatedSprite

onready var hitBox = $Hitbox
onready var accionBox = $Accion_hurtbox
#onready var detectionBox = $detection_box
onready var hurtBox = $Hit_Hurtbox
onready var softCollision = $SoftCollision

#TODO: for animation.tree
#onready var animationState = animationTree.get("parameters/playback")

func _ready():
	isDead = false
	
	print_debug(stats.max_health)
	print_debug(stats.health)


func _physics_process(delta):
	knockback = knockback.move_toward(Vector2.ZERO, FRICTION * delta)
	knockback = move_and_slide(knockback)
	
	var player = playerDetectionZone.player
	var no_movement = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
	
	
	match state:
		IDLE:
			velocity = no_movement;
			seek_player()
			
		WANDER:
			pass
			
		CHASE:
			if player != null:
				var direction = (player.global_position - global_position).normalized()
				velocity = velocity.move_toward(direction * MAX_SPEED, ACCELERATION * delta)
			else:
				state = IDLE
				
			animatedSprite.flip_h = velocity.x < 0
			
		DEAD: 
			# TODO: cambiar animacion aca
			isDead = true
			velocity = no_movement
			
			_on_Stats_no_health();
			
			animatedSprite.visible = false
			
	
	if softCollision.is_collading():
		velocity += softCollision.get_push_vector() * delta * 600
		
	velocity = move_and_slide(velocity)


func seek_player():
	if playerDetectionZone.can_see_player():
		state = CHASE

func _on_Accion_hurtbox_area_entered(area):
	#TODO: Implement accion event. 
	#What happens when player trigger accion on enemy?
	
	print_debug("accion triggered on enemy")
	queue_free()

func _on_Hit_Hurtbox_area_entered(area):
	create_hit_effect()
	knockback = area.knockback_vector * 100
	stats.health -= area.damage

func create_hit_effect():
	print_debug("hit_effect on enemy triggered")
	var EnemyHitEffectScene = load("res://effects/enemy_hit_effect.tscn")
	var EnemyHitEffectNode = EnemyHitEffectScene.instance()
	
	var world = get_tree().current_scene
	world.add_child(EnemyHitEffectNode) 
	
	EnemyHitEffectNode.global_position = global_position

func _on_Stats_no_health():
		#animatedSprite.play("deadx");
	
	#TODO:  
	# - call function to set animation on death to vector point
	# - turn off colissions
	
	state = DEAD
	
	var enemyDeathEffect = EnemyDeathEffect.instance()
	get_parent().add_child(enemyDeathEffect)
	enemyDeathEffect.position = global_position
	
	$Hitbox/CollisionShape2D.disabled = true
	

