extends Area2D

export(bool) var show_hit = true

#TODO: check if implement different effects depending on what variable as parameter is given
#enemy, plant, enemy8?

const HitEffect = preload("res://effects/hit_effect.tscn")

func _on_Hurtbox_area_entered(area):
	if show_hit:
		var effect = HitEffect.instance()
		var main = get_tree().current_scene
		main.add_child(effect)
		effect.global_position = global_position
